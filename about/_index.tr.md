---
title: "Hakkımda"
---

:wave: Merhaba! Ben Eyüp Can ELMA, yazılım geliştiriciyim. Tebrikler, bu harika bloga ulaştınız! :smile: 

:pen: Bu internet sitem, yazdığım yazıları servis ediyor. Bazen tecrübe ettiklerim, bazen ise üzerinde çalıştığım işler hakkında yazıyorum. 2020 yılının geri kalanı için daha fazla yazı yazmayı planlıyorum, çünkü öğrendiğim ve anlatmak istediğim çok şey var.

:computer: Freelance olarak yazılım geliştiriyor, problem çözüyorum. Son zamanlarda Upwork üzerinde freelancer olarak çalışmaktaydım ve aldığım işlerin geneli otomasyon ile sohbet botları üzerineydi. Hemen sonrasında ise okuldan kalan zamanımda, Türkiye'deki yazılım sektörünü tanımak ve tecrübe kazanmak için bir yazılım şirketinde 6 ay kadar çalıştım. 2020 yılında mezun oldum. Şu anda ise değerlendirmek istediğim teklifler olunca çalışıyor, aksi takdirde yeni projeler geliştirmeye ve alanımda yeni bilgiler öğrenmeye vakit ayırıyorum.

:kite: [Telegramic]({{< relref "/work/telegramic" >}}), son zamanlarda gerçekten keyif alarak geliştirdiğim web uygulamalarımdan biri. 2020 itibarıyla 65 bin kullanıcısı var ve her gün yaklaşık 150 yeni insanın katıldığı kullanışlı bir araç setine ve topluluk ağına dönüştü. Telegramic hakkındaki detaylı bir yazıma [buraya]({{< relref "/notes/telegramic" >}}) tıklayarak ulaşabilirsiniz. 

:penguin: GNU/Linux'e olan ilgim her geçen gün artıyor. Sistem güvenliği, optimizasyonu ve yönetimi üzerine kendimi geliştiriyorum. Veri güvenliği benim için artık bir hobi sayılır, zira çokça ilgileniyor ve bundan keyif alıyorum. Tamamen, şeytanın kök salmamasını sağlamaya yönelik bir ilgiye dönüşmek üzere ;). Son zamanlarda [Lockigest]({{< relref "/notes/lockigest" >}}) isminde güvenlik amaçlı bir betik yazdım. Arch Linux kullanıyorum ve kullanmanızı tavsiye ediyorum; [kurulumlarınızı kolaylaştıracak bir betik](https://github.com/elmsec/encrypted-arch) de yazdım.

:car: Rutin hale gelen işleri yazılımlarla otomatikleştirmek veya var olan bir yapıyı iyileştirmekten de çok keyif alıyorum. Tüm günümü bu şekilde harcadığım çok olmuştur. 

:robot: Semantik yanı olan ve insanla insanın dilinde iletişim kuran yapıları seviyorum. Bu yüzden olsa gerek, sohbet botları çok cazip geliyor ve hemen her birkaç ayda bir, bu konuda yeni bir proje tamamlıyorum. Bundan sonra sıradanlıktan kurtulup, işleri **yapay** da olsa daha **zeka**lı hale getirmek istiyorum.

:art: Yazılımın kahvehane muhabbeti de yazılım dilleri üzerinedir, ben de bir değineyim :smile:. Yeni diller denemek ve onları tanımak faydalı ve güzel, ben de sıklıkla yapıyorum. Son iki yıl içerisinde kullandığım ve bir tane de olsa proje geliştirdiğim diller ve çatılar; Python, Django, Flask, Tornado; Javascript, React, ReactJS, VueJS; Dart, Flutter; Java, Android; PHP, CodeIgniter; Bash?:D. En pratik bulduğum Python, en ilginç bulduğum Dart ve Javascript, en /ayrıntılı/ ve /ciddi!/ bulduğum ise Java. Şimdi ise gözüme Go ve Rust'ı kestirdim.




