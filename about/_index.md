---
title: "About"
date: "Jul 23, 1996"
---
Hi everyone, nice to meet you all.

I'm a code-lover human _from Turkey_. There are a lot of things I interested in, but my favorites are GNU/Linux; for sure, then Python and Django come up from behind it. I like to learn new things, especially if it's about computers.  

I'll completely introduce myself, soon. So, you'll be able to know all about my interests related to the world of computers, of course if you wanna know them.
