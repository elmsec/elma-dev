---
title: "Lisans"
---

Faydalı bir amaç uğruna olduğu müddetçe, paylaştığım her yazıyı dilediğiniz gibi kullanabilirsiniz. Haklarınızın korunması için atıfta bulunmanız daha doğru olur ama çok da şey yapmamak lazım.
