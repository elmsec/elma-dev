---
title: "License"
---

As long as it is for a useful purpose, you can use any article I share as you wish. It would be better to refer to me in order to protect your rights.
