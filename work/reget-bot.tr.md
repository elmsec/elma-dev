---
title: "Reget Bot (Telegram Bot)"
date: 2018-06-18T19:41:34+03:00
draft: false
---

Reget Bot; yeni müzikler, filmler, kitaplar, yazarlar, oyunlar, diziler ve TV programları keşfetmeniz için size yardım eder.

Tüm istekleri __Python__ ve [Telegram Bots API](https://core.telegram.org/bots/api) ile ele alır. Kendisi hakkında [şurada]({{< relref path="/notes/reget-bot" lang="tr">}}) detaylıca yazdım.

- [:cat: Github'da görüntüle](https://github.com/canelma/regetbot)  
- [:kite: Telegramic'te görüntüle](https://telegramic.org/bot/regetbot)
- [:speak_no_evil: Telegram'da konuş](https://t.me/regetbot)
- [:stars: Destekle](https://t.me/tlgrmcbot?start=regetbot-review)
