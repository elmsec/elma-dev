---
title: "Reget Bot (Telegram Bot)"
date: 2018-06-18T19:41:34+03:00
draft: false
---

Reget Bot works based on [Tastekid API](https://tastedive.com/read/api) and can help you discover new musics, movies, books, authors, games or TV shows and series. It's written in Python.

It also uses the [Telegram Bots API](https://core.telegram.org/bots/api) as an interface.

- [:cat: Github repository](https://github.com/canelma/regetbot)   
- [:kite: View on Telegramic](https://telegramic.org/bot/regetbot)
- [:speak_no_evil: Talk on Telegram](https://t.me/regetbot)
- [:stars: Support it](https://t.me/tlgrmcbot?start=regetbot-review)
